package sortersTesterClasses;

import sorterClasses.BubbleSortSorter;

public class ExerciseTwo {

	public static void main(String[] args) {

		
		BubbleSortSorter<Integer> sorter = new BubbleSortSorter<Integer>();
		Integer array[] = {5,9,20,22,20,5,4,13,17,8,22,1,3,7,11,9,10};
		
		System.out.println("Original: ");
		for(int i = 0; i<array.length; i++) {
			System.out.println(array[i]);
		}
		
		sorter.sort(array, new IntegerComparator1());
		System.out.println("Increasing: ");
		for(int i = 0; i<array.length; i++) {
			System.out.println(array[i]);
		}
		
		sorter.sort(array, new IntegerComparator2());
		System.out.println("Decreasing: ");
		for(int i = 0; i<array.length; i++) {
			System.out.println(array[i]);
		}
		
	}

}

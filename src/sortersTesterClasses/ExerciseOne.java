package sortersTesterClasses;

import sorterClasses.BubbleSortSorter;

public class ExerciseOne {

	public static void main(String[] args) {
		BubbleSortSorter<Entero> sorter = new BubbleSortSorter<Entero>();
		Entero array[] = new Entero[6];
		array[0] = new Entero (1); array[1] = new Entero(4); array[2] = new Entero (0);
		array[3] = new Entero (10); array[4] = new Entero(8); array[5] = new Entero(22);
		
		System.out.println("Original: ");
		for(int i = 0; i<array.length; i++) {
			System.out.println(array[i]);
		}
		
		sorter.sort(array, null);
		System.out.println("Sorted: ");
		for(int i = 0; i<array.length; i++) {
			System.out.println(array[i]);
		}

	}

}
